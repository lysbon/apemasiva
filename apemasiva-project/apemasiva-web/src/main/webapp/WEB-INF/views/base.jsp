<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"></c:set>

<spring:url value="/resources/core/css/main.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<spring:url value="/resources/core/css/bootstrap-select.min.css" var="bootstrapSelectCss" />
<spring:url value="/resources/core/css/narrow-jumbotron.css" var="narrowCss" />
<spring:url value="/resources/core/css/jquery.dataTables.min.css" var="dataTableCss"/>
<spring:url value="/resources/core/css/dataTables.bootstrap.min.css" var="dataTableBootCss"/>
<link href="${dataTableCss}" rel="stylesheet" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${bootstrapSelectCss}" rel="stylesheet" />
<link href="${dataTableBootCss}" rel="stylesheet" />
<link href="${narrowCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />

<spring:url value="/resources/core/js/util.js" var="utilJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<spring:url value="/resources/core/js/bootstrap-select.min.js" var="bootstrapSelectJs" />
<spring:url value="/resources/core/js/jquery.min.js" var="jqueryJs" />
<spring:url value="/resources/core/js/jquery.dataTables.min.js" var="dataTableJs" />
<spring:url value="/resources/core/js/dataTables.bootstrap.min.js" var="dataTableBootJs" />

<script src="${jqueryJs}"></script>
<script src="${dataTableJs}"></script>
<script src="${utilJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${bootstrapSelectJs}"></script>

<spring:url value="/resources/core/js/main.js" var="mainJs" />
<spring:url value="/resources/core/js/item.js" var="itemJs" />
<spring:url value="/resources/core/js/kits.js" var="kitsJs" />
<spring:url value="/resources/core/js/plantilla.js" var="plantillaJs" />
<spring:url value="/resources/core/js/plantillas.js" var="plantillasJs" />
<spring:url value="/resources/core/js/recursos.js" var="recursosJs" />
<spring:url value="/resources/core/js/seccion.js" var="seccionJs" />
