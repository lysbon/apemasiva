<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Aperturas Masivas</title>

<%@include file="base.jsp"%>

</head>

<body>	

<%@include file="header.jsp"%>

<div class="container">
	
</div>

<script src="${mainJs}"></script>

</body>
</html>