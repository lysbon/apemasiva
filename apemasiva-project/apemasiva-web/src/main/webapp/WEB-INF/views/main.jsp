<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>

<%@include file="base.jsp"%>

</head>

<body>	

<%@include file="header.jsp"%>

<div class="container">
	<div class="jumbotron">

		<h1 class="display-3">Env�o de contratos</h1>
		<p class="lead">Herramienta que permite la generaci&oacute;n din&aacute;mica de pdf con certificado
			digital, env&iacute;o de correo y descarga a FileUnico.
		</p>
		
		<div>
			<form action="inicio" method="post">
				<div class="form-group">
					<label for="selectEnviroment" class="control-label">Ambiente</label>
					<select class="form-control" id="selectEnviroment" name="selectEnv">
						<c:forEach var="env" items="${envs}">
	        				<option value="${env.cdEnv}" ${env.cdEnv == envDefault ? 'selected="selected"' : ''}>${env.nbNombre}</option>
	    				</c:forEach>
					</select>
				</div>	
						
				<br/>			
				<button type="submit" class="btn btn-lg btn-success" href="#" role="button" id="btnIngresar">Ingresar</button>
			</form>			
		</div>
			
		</p>
	</div>
</div>

<script src="${mainJs}"></script>

</body>
</html>