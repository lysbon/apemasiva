function dataTableDefecto(id){
	return dataTable(id,7);
}

function validateFilename(filename){
	return /^[a-z0-9_.@()-]+\.[^.]+$/i.test(filename.value);
}

function destruirTabla(id){
	if ($.fn.dataTable.isDataTable( id ) ) {
		$(id).DataTable().destroy();
	}
	$(id+' tbody').html('');
}

function dataTable(id,numrows){
	
	if ($.fn.dataTable.isDataTable( id ) ) {
		$(id).DataTable().destroy();
	}
	
	return $(id).DataTable({
		    	"ordering"    : false,
		    	"lengthChange": false,
		    	"pageLength"  : numrows,
		        "info"        : false,
		        "bFilter"     : false,
		        "bInfo"       : false,
		        "sSearch"     : "Buscar: ",
		        "language"    : {"paginate": 
		        					{
		        	      				"previous": "Ant.",
		        	      				"next": "Sig.",
		        	    		 	}
		        	  			}
    });
}

function check(flag){
	if(flag == 'A'){
		return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
	}else{
		return '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	}
}