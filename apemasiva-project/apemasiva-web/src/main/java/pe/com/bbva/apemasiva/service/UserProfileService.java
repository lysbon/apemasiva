package pe.com.bbva.apemasiva.service;

import java.util.List;

import pe.com.bbva.apemasiva.model.UserProfile;
 
public interface UserProfileService {
 
    UserProfile findById(int id);
 
    UserProfile findByType(String type);
     
    List<UserProfile> findAll();
     
}