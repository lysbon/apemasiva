package pe.com.bbva.apemasiva.dao;

import java.util.List;

import pe.com.bbva.apemasiva.model.User;


public interface UserDao {
 
    User findById(int id);
     
    User findBySSO(String sso);
     
    void save(User user);
     
    void deleteBySSO(String sso);
     
    List<User> findAllUsers();
 
}