package pe.com.bbva.apemasiva.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import com.grupobbva.bc.per.tele.seguridad.ServiciosSeguridadBbva;

public class BBVALDAPFilter extends AbstractPreAuthenticatedProcessingFilter{

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		ServiciosSeguridadBbva ssBbva = new ServiciosSeguridadBbva(request);
		String principal = "";
		try {
			ssBbva.obtener_ID();
			principal = ssBbva.getUsuario();
		} catch (Exception e) {
			principal = null;
		}
		if(principal==null){
			//throw new PreAuthenticatedCredentialsNotFoundException("LDAP header not found in request.");
			String user = (String)request.getAttribute("idm_user");
			if(!StringUtils.isEmpty(user)) return user;
			
			principal = request.getHeader("idm_user");
		
		}
		return principal;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		return "N/A";
	}
	
}
