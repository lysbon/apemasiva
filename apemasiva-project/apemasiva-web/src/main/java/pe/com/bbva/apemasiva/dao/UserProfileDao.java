package pe.com.bbva.apemasiva.dao;

import java.util.List;

import pe.com.bbva.apemasiva.model.UserProfile;
 
public interface UserProfileDao {
 
    List<UserProfile> findAll();
     
    UserProfile findByType(String type);
     
    UserProfile findById(int id);
}